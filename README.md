# Pilot Care Front End

Static HTML files for Pilot Care Org.

## Viewing the site

You need local server for the site to load correctly. The quickest way is to set an alias in WAMP.

## Compiling Sass

Install Node-Sass and Edje globally if you haven't. Open command prompt as admin and type:

```
npm install -g node-sass
npm install -g edje
```

Open a command prompt in your project directory and type `npm run sass` to start compiling.

Visit [here](https://github.com/hrsetyono/edje/wiki#installation) for more detail.
