(function($) {

  $('#registerform').on('click', '.acf-error', function() {
    $(this).find('.acf-error-message').fadeOut();
    $(this).removeClass('acf-error');
  });

})(jQuery);
