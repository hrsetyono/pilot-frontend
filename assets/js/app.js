(function($) {

/*
  Generic Listeners
*/
var myApp = {
  init: function() {
    this.initInclude();
    this.initDatepicker();
    this.toggleBelow();
    this.toggleMenu();

    // fancybox
    $.fancybox.defaults.touch = 'false';
  },

  initInclude: function() {
    $('[data-include]').each(function() {
      var page = $(this).data('include');
      $(this).load(page);
    });
  },

  initDatepicker: function() {
    $('.input-date').pikaday({
      format: 'D MMM YYYY',
    });
  },

  /*
    Toggle the other element within the container AND hide the button
  */
  toggleBelow: function() {
    $(document).on('click', '.toggle-below', function(e) {
      $(this).hide().siblings('*').slideToggle();
    });
  },

  toggleMenu: function () {
    $(document).on('click', '.toggle', function (e) {
      $('.nav--mobile').slideToggle();
    });
  },

   
};

/*
  Handle FAQ page
*/
var myFaq = {
  init: function() {
    // only run on page that has tabs
    if($('.tabs').length <= 0) { return false; }

    $('.tabs label').on('click', this.openTab);
    $('label.tabs__header--mobile').on('click', this.toggleTab);
  },

  openTab: function(e) {
    var $tabLabel = $(this);
    var $tabContent = $('.tab-content#' + $tabLabel.attr('for'));

    // add and remove active class
    $tabLabel.addClass('active');
    $tabLabel.siblings('label').removeClass('active');

    $tabContent.addClass('tab-active');
    $tabContent.siblings('.tab-content').removeClass('tab-active');
  },

  toggleTab: function(e) {
    var $tabLabel = $(this);
    var $tabContent = $('.tab-content#' + $tabLabel.attr('for'));

    $tabLabel.toggleClass('active');
    $tabContent.slideToggle();
  }
}

/*
  Handle front-end form
*/
var myForm = {
  init: function() {
    var self = this;

    $(document).on('wpcf7mailsent ', this.onChequeSuccess);
  },

  /*
    Hide the cheque submission form on success
  */
  onChequeSuccess: function(e) {
    if(e.detail.contactFormId != 379) { return false; }

    var $form = $('#' + e.detail.id + ' form');
    $form.find('.wpcf7-fields-wrap').slideToggle();
  },
};

// What to do after document is ready
function start() {
  myApp.init();
  myFaq.init();
  myForm.init();
}

$(document).ready(start);
$(document).on('page:load', start);

// Browser compatibility, leave this untouched
if('registerElement' in document) { document.createElement('h-row'); document.createElement('h-column'); }

})(jQuery);
